This docker can be used for development 
<br>
- Investree Legacy
<br>
- API Investree Legacy

<strong>Requirement</strong>
<br>
- Put this setting in the same folder with other repo (investree, api-investree) see image below
<br>
![Screenshot](images/folder.png)

<strong>How to use</strong>
<br>
- Run docker-compose up inside this repo

<br>
If all docker is downloaded and process up and running here is how to access: 
<br>
For legacy investree run in port 3000 (http://localhost:3000)
<br>
For api investree legacy run in port  8000 (http://localhost:8000/{api_prefix})